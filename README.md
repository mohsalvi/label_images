# label_images
Set of codes for labeling images for image classification tasks

## label_images.py

Binary labeling of images from a folder. Left and Right arrow keys are used to define the class.
Usage: python label_images.py -f [folder containing images]

Requirements:
matplotlib==1.5.1
Pillow==3.2.0
