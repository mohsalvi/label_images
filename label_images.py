from PIL import Image
import matplotlib.pyplot as plt
import argparse
import os


def test_keys(event):
        print event.key


def full_paths(image_folder):
    '''
    Function to create full image paths:
    concatenates image folder with image names
    '''
    image_list = []
    tuple_of_extensions = ('jpg', 'jpeg', 'tif', 'png', 'bmp')
    for file in os.listdir(image_folder):
        if file.endswith(tuple_of_extensions):
            image_list.append(file)

    # image names joined with folder name
    image_full_paths = []
    for i in image_list:
        image_full_paths.append(os.path.join(image_folder, i))

    return image_full_paths


def class_images(event):

    'toggle the visible state of the two images'
    if event.key == 'right':
        os.rename(image,
                  false_folder + '/{}.jpg'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'txt'):
            os.rename(image[:-3] + 'txt',
                      false_folder + '/{}.txt'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'mat'):
            os.rename(image[:-3] + 'mat',
                      false_folder + '/{}.mat'.format(image[:-3]))

    elif event.key == 'left':
        os.rename(image,
                  true_folder + '/{}.jpg'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'txt'):
            os.rename(image[:-3] + 'txt',
                      true_folder + '/{}.txt'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'mat'):
            os.rename(image[:-3] + 'mat',
                      true_folder + '/{}.mat'.format(image[:-3]))

    elif event.key == 'down' or event.key == 'up':
        os.rename(image,
                  bad_folder + '/{}.jpg'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'txt'):
            os.rename(image[:-3] + 'txt',
                      bad_folder + '/{}.txt'.format(image[:-3]))
        if os.path.exists(image[:-3] + 'mat'):
            os.rename(image[:-3] + 'mat',
                      bad_folder + '/{}.mat'.format(image[:-3]))


if __name__ == "__main__":

    # Arguments:
    parser = argparse.ArgumentParser(description='Bing Image Search')
    parser.add_argument('-f', '--input_folder',
                        help='folder containing images to annotate')
    parser.add_argument('--restart',
                        help='Start from beginning of folder',
                        action='store_true', required=False)
    args = parser.parse_args()

    print('LEFT (TRUE) -or- RIGHT (FALSE)')

    true_folder = args.input_folder + '_true'
    false_folder = args.input_folder + '_false'
    bad_folder = args.input_folder + '_bad'

    if not os.path.exists(true_folder):
        os.makedirs(true_folder)
    if not os.path.exists(false_folder):
        os.makedirs(false_folder)
    if not os.path.exists(bad_folder):
        os.makedirs(bad_folder)

    image_list = full_paths(args.input_folder)

    for i, image in enumerate(image_list):
        print ('Image {} of {}'.format(i, len(image_list)))
        x1 = Image.open(image_list[i])
        im1 = plt.imshow(x1, extent=(0, 1, 0, 1))
        plt.connect('key_press_event', class_images)

        plt.draw()
        plt.waitforbuttonpress()
